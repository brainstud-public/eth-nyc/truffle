import React from 'react';
import classNames from 'classnames/bind';
import styles from './Progress.module.css';
import { EPBar } from '../../components/EPBar/EPBar';
import { Ticker } from '../../components/Ticker/Ticker';

const classes = classNames.bind(styles);

/**
 * Progress.
 *
 * Shows the progress section
 */
export const Progress = ({ className, style }) => {

  return (
    <section className={classes(styles.base, className)} style={style}>
      <EPBar percentage={30}/>
      <Ticker amount={50}/>
    </section>
  );
};
