import { customHttpProvider, superfluidConfiguration } from "../../superfluid-config";
import { Framework } from "@superfluid-finance/sdk-core";
import { ethers } from "ethers";

export default class SuperfluidManager {
    constructor(address) {
        this.address = address;
    }

    async initialise() {
        console.log('creating');
        this.sf = await Framework.create({
            networkName: superfluidConfiguration.networkName,
            resolverAddress: superfluidConfiguration.resolverAddress,
            customSubgraphQueriesEndpoint: superfluidConfiguration.customSubgraphQueriesEndpoint,
            provider: customHttpProvider
        });

        this.signer = this.sf.createSigner({
            privateKey: superfluidConfiguration.senderPrivateKey,
            provider: customHttpProvider
        });

        this.DAIxContract = await this.sf.loadSuperToken("fDAIx");
        this.DAIx = this.DAIxContract.address;
    }

    async getFlowBalance() {
        let realtimeBalanceData = await this.DAIxContract.realtimeBalanceOf({
            providerOrSigner: this.signer,
            account: this.address,
        });

        let realtimeFlowRate = await this.sf.cfaV1.getFlow({
            superToken: this.DAIx,
            sender: superfluidConfiguration.senderAddress,
            receiver: this.address,
            providerOrSigner: this.signer,
        });


        let balanceObject = {
            initialTimestamp: new Date(realtimeBalanceData.timestamp),
            initialBalance: parseInt(realtimeBalanceData.availableBalance),
            currentFlowRate: parseInt(realtimeFlowRate.flowRate),
            realtimeBalance: (parent) => {
                let currentDate = new Date();
                let secondsDifference = ( currentDate.getTime() - parent.initialTimestamp.getTime() ) / 1000;
                secondsDifference = Math.floor(secondsDifference);
                return parent.initialBalance + (secondsDifference * parent.currentFlowRate);
            }
        }

        return balanceObject;
    }

    async createFlow(flowRate) {
        try {
            const createFlowOperation = this.sf.cfaV1.createFlow({
                flowRate: flowRate,
                receiver: this.address,
                superToken: this.DAIx
            });

            console.log("Creating new stream...");
            const result = await createFlowOperation.exec(this.signer);
            console.log("Stream created.");

            return result;
        } catch (error) {
            console.log("Transaction returned an error. Ensure the stream is unique and the address is valid.");
            console.error(error);
        }
    }

    async updateFlow(flowRate) {
        try {
            const updateFlowOperation = this.sf.cfaV1.updateFlow({
              flowRate: flowRate,
              receiver: this.address,
              superToken: this.DAIx
              // userData?: string
            });

            console.log("Updating stream...");
            const result = await updateFlowOperation.exec(this.signer);
            console.log("Stream updated");

            return result;
        } catch (error) {
            console.log("Transaction returned an error. Ensure the stream exists and the address is valid.");
            console.error(error);
        }
    }

    async deleteFlow() {
        try {
            const deleteFlowOperation = this.sf.cfaV1.deleteFlow({
              receiver: this.address,
              superToken: this.DAIx,
              sender: superfluidConfiguration.senderAddress,
              // userData?: string
            });

            console.log("Deleting stream...");
            const result = await deleteFlowOperation.exec(this.signer);
            console.log("Stream deleted.");
            return result;
        } catch (error) {
            console.log("Transaction returned an error. Ensure the stream exists.");
            console.error(error);
        }
    }

    calculateFlowRate(amountInEther) {
        if (
          typeof Number(amountInEther) !== "number" ||
          isNaN(Number(amountInEther)) === true
        ) {
            console.log(typeof Number(amountInEther));
        } else if (typeof Number(amountInEther) === "number") {
            const monthlyAmount = ethers.utils.parseEther(amountInEther.toString());
            const calculatedFlowRate = Math.floor(monthlyAmount / 3600 / 24 / 30);
            return calculatedFlowRate;
        }
    }
}
