import { customHttpProvider, superfluidConfiguration } from "../../superfluid-config";
import { Framework } from "@superfluid-finance/sdk-core";
import { ethers } from "ethers";

export default class SuperfluidManager {
  constructor(address) {
    this.address = address;
  }

  async initialise() {
    this.sf = await Framework.create({
      chainId: 69,
      //networkName: superfluidConfiguration.networkName,
      resolverAddress: '0x218B65780615Ff134f9Ad810CB98839534D3C0D6', //superfluidConfiguration.resolverAddress,
      //customSubgraphQueriesEndpoint: superfluidConfiguration.customSubgraphQueriesEndpoint,
      provider: customHttpProvider
    });

    this.signer = this.sf.createSigner({
      privateKey: superfluidConfiguration.senderPrivateKey,
      provider: customHttpProvider
    });

    this.DAIxContract = await this.sf.loadSuperToken("0x42F246132820145cdcC487f0EBf84bAfa69744F7");

    /** APPROVE DEM SPEND */
    let amt = 160;
    /*
            const DAI = new ethers.Contract(
              "0xCdA61194672DeF2f44243eD7dAF590536dbB2fC1",
              [
                  {
                      "inputs": [
                          {
                              "internalType": "string",
                              "name": "name_",
                              "type": "string"
                          },
                          {
                              "internalType": "string",
                              "name": "symbol_",
                              "type": "string"
                          }
                      ],
                      "stateMutability": "nonpayable",
                      "type": "constructor"
                  },
                  {
                      "anonymous": false,
                      "inputs": [
                          {
                              "indexed": true,
                              "internalType": "address",
                              "name": "owner",
                              "type": "address"
                          },
                          {
                              "indexed": true,
                              "internalType": "address",
                              "name": "spender",
                              "type": "address"
                          },
                          {
                              "indexed": false,
                              "internalType": "uint256",
                              "name": "value",
                              "type": "uint256"
                          }
                      ],
                      "name": "Approval",
                      "type": "event"
                  },
                  {
                      "anonymous": false,
                      "inputs": [
                          {
                              "indexed": true,
                              "internalType": "address",
                              "name": "from",
                              "type": "address"
                          },
                          {
                              "indexed": true,
                              "internalType": "address",
                              "name": "to",
                              "type": "address"
                          },
                          {
                              "indexed": false,
                              "internalType": "uint256",
                              "name": "value",
                              "type": "uint256"
                          }
                      ],
                      "name": "Transfer",
                      "type": "event"
                  },
                  {
                      "inputs": [
                          {
                              "internalType": "address",
                              "name": "owner",
                              "type": "address"
                          },
                          {
                              "internalType": "address",
                              "name": "spender",
                              "type": "address"
                          }
                      ],
                      "name": "allowance",
                      "outputs": [
                          {
                              "internalType": "uint256",
                              "name": "",
                              "type": "uint256"
                          }
                      ],
                      "stateMutability": "view",
                      "type": "function"
                  },
                  {
                      "inputs": [
                          {
                              "internalType": "address",
                              "name": "spender",
                              "type": "address"
                          },
                          {
                              "internalType": "uint256",
                              "name": "amount",
                              "type": "uint256"
                          }
                      ],
                      "name": "approve",
                      "outputs": [
                          {
                              "internalType": "bool",
                              "name": "",
                              "type": "bool"
                          }
                      ],
                      "stateMutability": "nonpayable",
                      "type": "function"
                  },
                  {
                      "inputs": [
                          {
                              "internalType": "address",
                              "name": "account",
                              "type": "address"
                          }
                      ],
                      "name": "balanceOf",
                      "outputs": [
                          {
                              "internalType": "uint256",
                              "name": "",
                              "type": "uint256"
                          }
                      ],
                      "stateMutability": "view",
                      "type": "function"
                  },
                  {
                      "inputs": [],
                      "name": "decimals",
                      "outputs": [
                          {
                              "internalType": "uint8",
                              "name": "",
                              "type": "uint8"
                          }
                      ],
                      "stateMutability": "view",
                      "type": "function"
                  },
                  {
                      "inputs": [
                          {
                              "internalType": "address",
                              "name": "spender",
                              "type": "address"
                          },
                          {
                              "internalType": "uint256",
                              "name": "subtractedValue",
                              "type": "uint256"
                          }
                      ],
                      "name": "decreaseAllowance",
                      "outputs": [
                          {
                              "internalType": "bool",
                              "name": "",
                              "type": "bool"
                          }
                      ],
                      "stateMutability": "nonpayable",
                      "type": "function"
                  },
                  {
                      "inputs": [
                          {
                              "internalType": "address",
                              "name": "spender",
                              "type": "address"
                          },
                          {
                              "internalType": "uint256",
                              "name": "addedValue",
                              "type": "uint256"
                          }
                      ],
                      "name": "increaseAllowance",
                      "outputs": [
                          {
                              "internalType": "bool",
                              "name": "",
                              "type": "bool"
                          }
                      ],
                      "stateMutability": "nonpayable",
                      "type": "function"
                  },
                  {
                      "inputs": [],
                      "name": "name",
                      "outputs": [
                          {
                              "internalType": "string",
                              "name": "",
                              "type": "string"
                          }
                      ],
                      "stateMutability": "view",
                      "type": "function"
                  },
                  {
                      "inputs": [],
                      "name": "symbol",
                      "outputs": [
                          {
                              "internalType": "string",
                              "name": "",
                              "type": "string"
                          }
                      ],
                      "stateMutability": "view",
                      "type": "function"
                  },
                  {
                      "inputs": [],
                      "name": "totalSupply",
                      "outputs": [
                          {
                              "internalType": "uint256",
                              "name": "",
                              "type": "uint256"
                          }
                      ],
                      "stateMutability": "view",
                      "type": "function"
                  },
                  {
                      "inputs": [
                          {
                              "internalType": "address",
                              "name": "to",
                              "type": "address"
                          },
                          {
                              "internalType": "uint256",
                              "name": "amount",
                              "type": "uint256"
                          }
                      ],
                      "name": "transfer",
                      "outputs": [
                          {
                              "internalType": "bool",
                              "name": "",
                              "type": "bool"
                          }
                      ],
                      "stateMutability": "nonpayable",
                      "type": "function"
                  },
                  {
                      "inputs": [
                          {
                              "internalType": "address",
                              "name": "from",
                              "type": "address"
                          },
                          {
                              "internalType": "address",
                              "name": "to",
                              "type": "address"
                          },
                          {
                              "internalType": "uint256",
                              "name": "amount",
                              "type": "uint256"
                          }
                      ],
                      "name": "transferFrom",
                      "outputs": [
                          {
                              "internalType": "bool",
                              "name": "",
                              "type": "bool"
                          }
                      ],
                      "stateMutability": "nonpayable",
                      "type": "function"
                  }
              ],
              this.signer
            );
            try {
                console.log("approving DAI spend");
                await DAI.approve(
                  "0x42F246132820145cdcC487f0EBf84bAfa69744F7",
                  ethers.utils.parseEther(amt.toString())
                ).then(function (tx) {
                    console.log(
                      `Congrats, you just approved your DAI spend. You can see this tx at https://kovan.etherscan.io/tx/${tx.hash}`
                    );
                });
            } catch (error) {
                console.error(error);
            }
    */
    /** UPGRADE DEM TOKENS */
    // let amt = 160;

    /*
    try {
        console.log(`upgrading $${amt} DAI to DAIx`);
        const amtToUpgrade = ethers.utils.parseEther(amt.toString());
        const upgradeOperation = this.DAIxContract.upgrade({
            amount: amtToUpgrade.toString()
        });
        const upgradeTxn = await upgradeOperation.exec(this.signer);
        await upgradeTxn.wait().then(function (tx) {
            console.log(
              `
    Congrats - you've just upgraded DAI to DAIx!
  `
            );
        });
    } catch (error) {
        console.error(error);
    }
*/
    /** END TRASH */

    // this.DAIxContract = await this.sf.loadSuperToken("ETx");
    console.log(this.DAIxContract);
    // this.DAIxContract = await this.sf.loadSuperToken("fDAIx");
    this.DAIx = this.DAIxContract.address;
  }

  async getFlowBalance() {
    let realtimeBalanceData = await this.DAIxContract.realtimeBalanceOf({
      providerOrSigner: this.signer,
      account: this.address,
    });

    if (realtimeBalanceData.availableBalance === "0") {
      await this.createFlow(1157407407407);
      let realtimeBalanceData = await this.DAIxContract.realtimeBalanceOf({
        providerOrSigner: this.signer,
        account: this.address,
      });
    }

    let realtimeFlowRate = await this.sf.cfaV1.getFlow({
      superToken: this.DAIx,
      sender: superfluidConfiguration.senderAddress,
      receiver: this.address,
      providerOrSigner: this.signer,
    });

    return {
      initialTimestamp: new Date(realtimeBalanceData.timestamp),
      initialBalance: parseInt(realtimeBalanceData.availableBalance),
      currentFlowRate: parseInt(realtimeFlowRate.flowRate),
      realtimeBalance: (parent) => {
        let currentDate = new Date();
        let secondsDifference = (currentDate.getTime() - parent.initialTimestamp.getTime()) / 1000;
        secondsDifference = Math.floor(secondsDifference);
        return parent.initialBalance + (secondsDifference * parent.currentFlowRate);
      }
    }
  }

  async createFlow(flowRate) {
    try {
      // let upgrade = await this.DAIxContract.upgrade({amount:ethers.BigNumber.from(100000000000)});
      // console.log(upgrade);
      // return;
      const createFlowOperation = this.sf.cfaV1.createFlow({
        flowRate: flowRate,
        receiver: this.address,
        superToken: this.DAIx
      });

      console.log("Creating new stream...");
      const result = await createFlowOperation.exec(this.signer);
      console.log("Stream created.");

      return result;
    } catch (error) {
      console.log("Transaction returned an error. Ensure the stream is unique and the address is valid.");
      console.error(error);
    }
  }

  async updateFlow(flowRate) {
    try {
      const updateFlowOperation = this.sf.cfaV1.updateFlow({
        flowRate: flowRate,
        receiver: this.address,
        superToken: this.DAIx
        // userData?: string
      });

      console.log("Updating stream...");
      const result = await updateFlowOperation.exec(this.signer);
      console.log("Stream updated");

      return result;
    } catch (error) {
      console.log("Transaction returned an error. Ensure the stream exists and the address is valid.");
      console.error(error);
    }
  }

  async deleteFlow() {
    try {
      const deleteFlowOperation = this.sf.cfaV1.deleteFlow({
        receiver: this.address,
        superToken: this.DAIx,
        sender: superfluidConfiguration.senderAddress,
        // userData?: string
      });

      console.log("Deleting stream...");
      const result = await deleteFlowOperation.exec(this.signer);
      console.log("Stream deleted.");
      return result;
    } catch (error) {
      console.log("Transaction returned an error. Ensure the stream exists.");
      console.error(error);
    }
  }

  calculateFlowRate(amountInEther) {
    if (
      typeof Number(amountInEther) !== "number" ||
      isNaN(Number(amountInEther)) === true
    ) {
      console.log(typeof Number(amountInEther));
    } else if (typeof Number(amountInEther) === "number") {
      const monthlyAmount = ethers.utils.parseEther(amountInEther.toString());
      const calculatedFlowRate = Math.floor(monthlyAmount / 3600 / 24 / 30);
      return calculatedFlowRate;
    }
  }
}
