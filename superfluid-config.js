export const Framework = require("@superfluid-finance/sdk-core");
export const ethers = require("ethers");

// Ethers.js provider initialization
export const url =
  "https://opt-kovan.g.alchemy.com/v2/E7DFDvnKldINjYKETzsXWXFHKlj_peGg";

export const customHttpProvider = new ethers.providers.JsonRpcProvider(url);

export const superfluidConfiguration = {
    networkName: 'optimism-kovan',
    resolverAddress: '0x218B65780615Ff134f9Ad810CB98839534D3C0D6s',
    customSubgraphQueriesEndpoint: 'https://api.thegraph.com/subgraphs/name/synthetixio-team/optimism-kovan-main',
    senderAddress: '0x13d577E6F7057937BA7ae0d654964fbC04503346',
    senderPrivateKey: '5344117370a9097c10830afb8917a80adaafe1bbaa177f6c8d12b41de60b093d',
    tokenAddress: '0x42F246132820145cdcC487f0EBf84bAfa69744F7',
};

/*
// GOERLI Configuration

export const url =
  "https://eth-goerli.alchemyapi.io/v2/zfWv3pEito9Wi7gDUSLsand11To5VEjN";

export const superfluidConfiguration = {
    networkName: 'goerli',
    resolverAddress: '0x3710AB3fDE2B61736B8BB0CE845D6c61F667a78E',
    customSubgraphQueriesEndpoint: 'https://thegraph.com/hosted-service/subgraph/superfluid-finance/protocol-v1-xdai',
    senderAddress: '0xDCB45e4f6762C3D7C61a00e96Fb94ADb7Cf27721',
    senderPrivateKey: '0xd2ebfb1517ee73c4bd3d209530a7e1c25352542843077109ae77a2c0213375f1',
};*/