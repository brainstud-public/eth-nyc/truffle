export const withSafeHydrate = (Component) => (props) => {
  return (
    <div suppressHydrationWarning>
      {typeof window === 'undefined' ? null : <Component {...props} />}
    </div>
  )
}
