import content from './content';

export default [
  {
    id: "f7819364-a426-4660-8296-17ba8211151d",
    title: "Ethereum",
    description: "Labore Lorem consectetur quis ea est sunt. Elit veniam eiusmod eu excepteur do non incididunt incididunt magna. Enim nulla et reprehenderit velit. Tempor aliqua ut excepteur excepteur consectetur commodo do ut amet nulla proident magna occaecat dolore. Lorem mollit ipsum sit. Ullamco eiusmod dolor non voluptate proident dolor. Eu labore elit sunt Lorem ea reprehenderit mollit excepteur est velit fugiat reprehenderit. Eiusmod elit anim sint est enim esse deserunt cillum velit fugiat labore fugiat. Anim fugiat excepteur dolor nulla reprehenderit. Officia nisi adipisicing veniam commodo eu exercitation velit quis enim veniam ex ad aute.",
    image: "http://placeimg.com/460/860/tech",
    chapters: content,
  },

  {
    id: "905a8cc3-390f-4412-bc5e-b258f767cb31",
    title: "Optimism",
    description: "Mollit anim cillum nisi aliquip Lorem consequat quis cupidatat proident velit cillum nisi. In cupidatat aliquip labore eiusmod velit nulla consequat. Dolore tempor labore labore consequat minim voluptate sunt et sunt ut aliqua eu. Sunt duis qui elit id id sint. Cupidatat enim elit labore fugiat culpa labore laboris sunt et qui Lorem ipsum ipsum velit. Consectetur ipsum in do esse non. Laborum elit dolor exercitation sunt aliquip cillum in amet consectetur do nulla magna dolor id. Sunt sit labore voluptate sint mollit. Excepteur nulla cillum nulla sint ex quis id anim elit ut proident. Ipsum eu consectetur consectetur ullamco fugiat pariatur voluptate est eiusmod ullamco ullamco officia sunt ex.",
    image: "http://placeimg.com/460/860/tech",
    chapters: content,
  },

  {
    id: "a82dac44-7224-45f9-b3d0-73280ae16bff",
    title: "Environmental Help",
    image: "http://placeimg.com/460/860/nature",
    chapters: content,
  },

  {
    id: "d7cc115d-075f-4ffa-acf8-cd06948df0e3",
    title: "Superfluid",
    description: "Tempor minim et ad commodo minim amet officia consequat. Consectetur in proident dolor magna esse magna commodo ipsum ipsum dolore in sunt exercitation voluptate. Exercitation reprehenderit consectetur magna proident veniam consectetur officia sint. Et consectetur nisi laboris in aliqua commodo laboris enim aliquip id consectetur occaecat duis velit ea. Lorem incididunt deserunt nulla deserunt occaecat. Officia aliqua cupidatat exercitation occaecat officia. Consectetur consectetur consequat voluptate cupidatat sunt fugiat est officia sit enim sint minim enim officia pariatur. Aute laborum ut irure excepteur. Lorem nulla amet dolor ipsum. Anim nulla aliquip nulla reprehenderit cillum ullamco cillum nisi dolor amet.",
    image: "http://placeimg.com/460/860/tech",
    chapters: content,
  },


]
