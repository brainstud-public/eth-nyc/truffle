export const excellent = {
  activeness: 10,
  distribution: 10,
  performance: 10,
  teamwork: 10,
  participation: 10,
  quality: 10,
  response: {
    scheme: 'success',
    message: 'Good job! Your stream is going faster now!',
  }
};

export const mediocre = {
  activeness: 6,
  distribution: 6,
  performance: 6,
  teamwork: 6,
  participation: 6,
  quality: 6,
  response: {
    scheme: 'warning',
    message: 'Alright! Keep this going and you will do good!',
  }
}

export const teamplayer = {
  activeness: 6,
  distribution: 2,
  performance: 1,
  teamwork: 9,
  participation: 3,
  quality: 8,
  response: {
    scheme: 'success',
    message: 'Alright, a mixed bag! Check the effect on the stream!',
  }
}

export const loner = {
  activeness: 9,
  distribution: 6,
  performance: 9,
  teamwork: 1,
  participation: 3,
  quality: 2,
  response: {
    scheme: 'warning',
    message: 'Oef, your stream could go faster!',
  }
}


export const worst = {
  activeness: 1,
  distribution: 1,
  performance: 1,
  teamwork: 1,
  participation: 1,
  quality: 1,
  response: {
    scheme: 'error',
    message: 'Oh oh! Now it is going very slow!',
  }
}
