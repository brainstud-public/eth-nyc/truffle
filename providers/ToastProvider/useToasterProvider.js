import { useContext } from 'react';
import { ToasterContext } from './ToasterContext';

export function useToasterProvider() {
  return useContext(ToasterContext);
}
