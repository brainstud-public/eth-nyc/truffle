import React, { useCallback, useEffect, useMemo, useRef, useState, } from 'react';
import classNames from 'classnames/bind';
import { ToasterContext } from './ToasterContext';
import styles from './ToasterProvider.module.css';


const cx = classNames.bind(styles);

const DEFAULT_SCHEME = 'primary';
const DEFAULT_DELAY = 5000;
const FADEOUT_ANIMATION_DURATION = 200;

/**
 * ToasterProvider
 *
 * Display a Toaster message to the user
 */
export const ToasterProvider = ({ children, duration = 5000 }) => {
  const [toast, setToast] = useState();
  const timer = useRef();
  const [fadingOut, setFadingOut] = useState();

  const createToast = useCallback((message, scheme = DEFAULT_SCHEME, options = {}) => {
    setToast({
      message,
      scheme,
      duration,
      ...options,
    });
  }, [duration]);

  const clearToasts = useCallback(() => {
    setToast(undefined);
  }, []);

  useEffect(() => {
    if ( timer.current ) {
      clearTimeout(timer.current);
      setFadingOut(false);
    }

    if ( toast ) {
      timer.current = window.setTimeout(() => setFadingOut(true), toast.duration || DEFAULT_DELAY);
    }

    return () => {
      if ( timer.current ) clearTimeout(timer.current);
    };
  }, [toast]);

  useEffect(() => {
    if ( fadingOut ) {
      timer.current = window.setTimeout(() => setToast(undefined), FADEOUT_ANIMATION_DURATION);
    }

    return () => {
      if ( timer.current ) clearTimeout(timer.current);
    };
  }, [fadingOut]);

  const methods = useMemo(() => [createToast, clearToasts], [createToast, clearToasts]);

  return (
    <ToasterContext.Provider value={methods}>
      {children}
      {toast && (
        <div className={cx(styles.toast, styles[toast.scheme || DEFAULT_SCHEME])}>
          {toast.message}
        </div>
      )}
    </ToasterContext.Provider>
  );
};
