import React from 'react';
import { QueryClientProvider } from 'react-query'
import { queryClient } from './QueryClient';


/**
 * ReactQueryProvider.
 */
export const ReactQueryProvider = ({ children }) => {

  return (
    <QueryClientProvider client={queryClient}>
      {children}
    </QueryClientProvider>
  );
};
