import React, { useState } from 'react';
import { BreadcrumbsContext } from './BreadcrumbsContext';
import { Container } from 'semantic-ui-react'
import Link from 'next/link';


/**
 * BreadcrumbsProvider.
 *
 * Provides a mechanism to
 */
export const BreadcrumbsProvider = ({ children }) => {
  const [breadcrumbs, setBreadCrumbs] = useState([]);

  return (
    <BreadcrumbsContext.Provider value={setBreadCrumbs}>
      {breadcrumbs.length > 0 && (
        <nav className="navigation">
          <Container>
            {breadcrumbs.map((crumb, index, arr) => (
              <React.Fragment key={crumb.content}>
                <Link key={crumb.content} href={crumb.href}>
                  <a>
                    {crumb.content}
                  </a>
                </Link>
                {index !== (arr.length - 1) && (
                  <svg width="8" height="8" viewBox="0 0 6 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path opacity="0.4" d="M2 1L4 3L2 5" stroke="#3C3C50" strokeLinecap="round" strokeLinejoin="round"/>
                  </svg>
                )}
              </React.Fragment>
            ))}
          </Container>
        </nav>
      )}

      {children}
    </BreadcrumbsContext.Provider>
  );
};
