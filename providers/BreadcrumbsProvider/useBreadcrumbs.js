/**
 * useBreadCrumbs.
 *
 * Creates an array of objects that can be used to render bread crumbs.
 */
import { useRouter } from 'next/router';
import { useContext, useEffect, useMemo } from 'react';
import { BreadcrumbsContext } from './BreadcrumbsContext';

export function useBreadcrumbs(data = {}) {
  const router = useRouter();
  const { asPath: uri } = router;
  const setBreadcrumbs = useContext(BreadcrumbsContext);
  const parts = uri.split('/');

  const map = useMemo(() => ({
    '': "Overview",
    course: 'Course',
    ...data
  }), [data]);

  useEffect(() => {
    setBreadcrumbs(parts.reduce((breadcrumbs, part, index, arr) => {
      const item = map[part];


      if ( item && breadcrumbs.every((item) => item.part !== part) ) {
        return typeof item === 'string' ? [
          ...breadcrumbs,
          {
            href: `/${arr.slice(0, index).join('/')}/${part}`.replace('//', '/'),
            key: item,
            content: item,
            part,
          },
        ] : [
          ...breadcrumbs,
          ...item.label ? [{
            content: item.label,
            key: item.label,
            part,
            ...(item.segment ? { href: `/${arr.slice(0, index).join('/')}/${item.segment}` } : {}),
          }] : [],
        ];
      }
      return breadcrumbs;
    }, []));
  }, [setBreadcrumbs, parts, map]);
}
