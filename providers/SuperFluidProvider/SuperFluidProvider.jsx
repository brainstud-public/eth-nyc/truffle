import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { SuperFluidContext } from './SuperFluidContext';
import SuperfluidManager from '../../backend/Superfluid/Manager';
import { useWeb3 } from '@3rdweb/hooks';
import { useLocalStorage } from '../../hooks/useLocalStorage';

/**
 * SuperFluidProvider.
 */
export const SuperFluidProvider = ({ children }) => {
  const [stream, setStream] = useState();
  const [balance, setBalance] = useState();
  const [flowRate, setFlowRate] = useState();
  const { address } = useWeb3();

  const [effectiveness, setEffectiveness] = useLocalStorage(`${address}-effectiveness`, 60);

  const loadManager = useCallback(async () => {
    const SFManager = new SuperfluidManager(address);
    await SFManager.initialise();
    setStream(await SFManager.getFlowBalance());
  }, [address]);


  useEffect(() => {
    if ( address ) {
      loadManager();
    }
  }, [address, loadManager]);

  const interval = useRef(null);
  useEffect(() => {
    if ( stream ) {
      setFlowRate(stream.currentFlowRate);
      interval.current = setInterval(() => {
        const newBalance = stream.realtimeBalance(stream);
        setBalance(newBalance);
      }, 100);
    }
    return () => {
      clearInterval(interval.current);
    }
  }, [stream]);

  const context = useMemo(() => ({
    isLoading: !balance,
    address,
    balance,
    effectiveness,
    setEffectiveness,
    loadManager,
    flowRate,
  }), [balance, flowRate, address, effectiveness, setEffectiveness, loadManager]);

  return (
    <SuperFluidContext.Provider value={context}>
      {children}
    </SuperFluidContext.Provider>
  );
};
