import { SuperFluidContext } from './SuperFluidContext';
import { useContext } from 'react';

export function useSuperFluid() {
  return useContext(SuperFluidContext);
}
