import { createContext } from 'react';

export const SuperFluidContext = createContext(null);
