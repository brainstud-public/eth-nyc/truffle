import SuperfluidManager from "../../backend/Superfluid/Manager";

const scoreWeights = {
  activeness: 20,
  distribution: 10,
  performance: 20,
  teamwork: 10,
  participation: 15,
  quality: 25
};

export default async (request, response) => {
  if (request.method !== 'PUT') {
    return response.status(422).json({
      error: 'Method not supported'
    });
  }

  let learnerAddress = request.body.address;
  const SFManager = new SuperfluidManager(learnerAddress);
  await SFManager.initialise();

  let learnerEffectivenessScore = 0;
  const submittedScores = request.body.result;

  for (const [key, weight] of Object.entries(scoreWeights)) {
    let currentScore = weight * submittedScores[key] / 100;
    learnerEffectivenessScore += currentScore;
    console.log(`${key}: ${currentScore}`);
  }

  learnerEffectivenessScore = learnerEffectivenessScore / 10;

  const maximumEth = 160;
  const eligibleEth = maximumEth * learnerEffectivenessScore;
  const flowRate = SFManager.calculateFlowRate(eligibleEth);

  await SFManager.updateFlow(flowRate);

  console.log(`Eligible ETH: ${eligibleEth} | FlowRate: ${flowRate}`);

  response.status(200).json({
    learnerEffectivenessScore: learnerEffectivenessScore * 100,
    newFlowRate: flowRate
  });
}
