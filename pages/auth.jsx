import Head from 'next/head';
import { Button, Card, Container } from 'semantic-ui-react';
import React, { useCallback, useEffect } from 'react';

import { useWeb3 } from '@3rdweb/hooks';
import { useRouter } from 'next/router';

const Auth = () => {

  const { connectWallet, address } = useWeb3();

  const handleLogin = useCallback(() => {
    connectWallet('injected');
  }, []);

  const router = useRouter();

  useEffect(() => {
    if ( address ) {
      router.push('/')
    }
  }, [address]);


  return (
    <Container>
      <Head>
        <title>
          Authorization
        </title>
      </Head>


      <Card fluid style={{ padding: '2rem', margin: '2rem 0' }}>

        <Button primary onClick={handleLogin}>
          Login with MetaMask
        </Button>

      </Card>


    </Container>
  );
}

export default Auth;
