import Head from 'next/head';
import { Button, Card, Container, FormField, Input } from 'semantic-ui-react';
import { useRouter } from 'next/router';
import axios from 'axios';
import { useMutation } from 'react-query';
import { useCallback } from 'react';
import { useBreadcrumbs } from 'providers/BreadcrumbsProvider/useBreadcrumbs';
import { Progress } from 'layouts/Progress/Progress';
import courses from 'db/courses';
import { useToasterProvider } from 'providers/ToastProvider/useToasterProvider';
import { useSuperFluid } from 'providers/SuperFluidProvider/useSuperFluid';


const AssignmentShow = () => {
  const router = useRouter()
  const { courseId, assignmentId, chapterId } = router.query

  const [setToast] = useToasterProvider();
  const { setEffectiveness, address, loadManager } = useSuperFluid();

  const course = courses.find((item) => item.id === courseId);
  const chapter = course?.chapters?.find((item) => item.id === chapterId);
  const assignment = chapter?.assignments?.find((item) => item.id === assignmentId);
  const putStream = useMutation(async (data) => {
    const result = await axios.put('/api/stream', data);
    setEffectiveness(result.data.learnerEffectivenessScore);
    loadManager();
    setToast(data.result.response.message, data.result.response.scheme);
  });

  useBreadcrumbs({
    [courseId]: course?.title,
    [assignmentId]: assignment?.title
  });

  const handleSubmit = useCallback(() => {
    putStream.mutate({
      address,
      result: assignment.result
    });
  }, [putStream]);

  return (
    <Container style={{ marginBottom: '4rem' }}>
      <Head>
        <title>
          {assignment?.title}
        </title>
      </Head>

      <Progress/>

      <div className={"flex-grid"}>
        <Card>
          <img
            src={assignment?.thumbnail}
            alt={""}
            className="assignment-image"
          />
          <p>{assignment?.text_left}</p>
        </Card>
        <Card className="panel">

          <h1>{assignment?.title}</h1>

          <p>{assignment?.text_right}</p>

          <FormField>
            <label>{assignment?.question}</label>
            <Input type={"text"} fluid/>
          </FormField>

          <Button
            primary
            loading={putStream.isLoading}
            onClick={handleSubmit}
            style={{ marginTop: '3rem' }}
          >
            Submit
          </Button>

        </Card>
      </div>
    </Container>

  );
};

export default AssignmentShow
