import { Fragment } from 'react';
import Head from 'next/head';
import { Card, Container, Message } from 'semantic-ui-react';
import { useRouter } from 'next/router';
import courses from 'db/courses';
import { AssignmentCard } from 'components/AssignmentCard/AssignmentCard';
import { useBreadcrumbs } from '../../../providers/BreadcrumbsProvider/useBreadcrumbs';
import { Progress } from '../../../layouts/Progress/Progress';

const CourseShow = () => {
  const router = useRouter()
  const { courseId } = router.query

  const course = courses.find((item) => item.id === courseId);

  useBreadcrumbs({
    [courseId]: course?.title,
  });

  return (
    <Container>
      <Head>
        <title>
          {course?.title}
        </title>
      </Head>

      <Progress/>

      <Card fluid className={"panel"}>

        <h1>{course?.title}</h1>

        <Message>
          Start learning by clicking one of the cards below.
        </Message>

        {course?.chapters?.map((chapter) => (
          <Fragment key={chapter.id}>
            <h2>{chapter.title}</h2>


            <div className="flex-grid">
              {chapter.assignments.map((assignment) => (
                <AssignmentCard
                  key={assignment.id}
                  id={assignment.id}
                  image={assignment.thumbnail}
                  chapterId={chapter.id}
                >
                  <h4>{assignment.title}</h4>
                </AssignmentCard>
              ))}
            </div>
          </Fragment>
        ))}
      </Card>

    </Container>
  );
}

export default CourseShow;
