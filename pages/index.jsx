import React from 'react';
import { Card, Container } from 'semantic-ui-react';
import { useAuth } from '../hooks/useAuth';
import courses from '../db/courses';
import { CourseCard } from '../components/CourseCard/CourseCard';
import { useBreadcrumbs } from '../providers/BreadcrumbsProvider/useBreadcrumbs';
import {UpdateFlow} from "./UpdateFlow";

/**
 * Main page
 */
const Index = () => {

  const address = useAuth();

  useBreadcrumbs();

  return (
    <Container as={"main"}>
      <Card fluid className="panel">
        <h1>Welcome!</h1>
        <p>Your account id: {address}</p>

        <div style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap' }}>
          {courses.map((course) => (
            <CourseCard key={course.id} id={course.id} image={course.image}>
              <h4>{course.title}</h4>
            </CourseCard>
          ))}
        </div>

      </Card>
    </Container>
  );
};


export default Index;
