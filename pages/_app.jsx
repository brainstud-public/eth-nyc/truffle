import React from 'react';
import 'semantic-ui-css/semantic.min.css'
import { ThirdwebWeb3Provider } from "@3rdweb/hooks";
import Link from 'next/link';
import "regenerator-runtime/runtime";
import { BreadcrumbsProvider } from '../providers/BreadcrumbsProvider/BreadcrumbsProvider';
import '../styles/styles.css';
import { ReactQueryProvider } from '../providers/ReactQueryProvider/ReactQueryProvider';
import { SuperFluidProvider } from '../providers/SuperFluidProvider/SuperFluidProvider';
import { ToasterProvider } from '../providers/ToastProvider/ToastProvider';


const App = ({ Component, pageProps }) => {
  const supportedChainIds = [80001, 4, 42, 5, 69];


  const connectors = {
    injected: {},
  };

  return (
    <ReactQueryProvider>
      <ThirdwebWeb3Provider
        supportedChainIds={supportedChainIds}
        connectors={connectors}
      >
        <SuperFluidProvider>
          <ToasterProvider>
            <header className="header">
              <Link href={"/"}>
                <a>
                  Earn While You Learn
                </a>
              </Link>
            </header>
            <BreadcrumbsProvider>
              <Component {...pageProps} />
            </BreadcrumbsProvider>
          </ToasterProvider>
        </SuperFluidProvider>
      </ThirdwebWeb3Provider>
    </ReactQueryProvider>
  )
}


export default App;
