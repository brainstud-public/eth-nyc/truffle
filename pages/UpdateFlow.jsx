import React, { useState } from "react";
import { ethers } from "ethers";
import SuperfluidManager from "../backend/Superfluid/Manager";

const learnerAddress = '0x03d391159ceACE45Dcd7816a7C07541436a9Ac11';


export const UpdateFlow = () => {

    const [flowData, setFlowData] = useState("");
    const [currentBalance, setCurrentBalance] = useState("0000000000000000000");
    const [currentFlowRate, setCurrentFlowRate] = useState("");

    async function getFlowInfo() {
        const SFManager = new SuperfluidManager(learnerAddress);
        await SFManager.initialise();

        // let createdThing = await SFManager.createFlow(ethers.BigNumber.from(61728395061728));
        let managedFlowData = await SFManager.getFlowBalance();
        setCurrentFlowRate(managedFlowData.currentFlowRate);
        setFlowData(managedFlowData);
        setCurrentFlowRate(parseInt(managedFlowData.currentFlowRate));
        setInterval(() => {
            setCurrentBalance(managedFlowData.realtimeBalance(managedFlowData));
        }, 1000);
    }

    async function increaseFlow() {
        const SFManager = new SuperfluidManager(learnerAddress);
        await SFManager.initialise();

        SFManager.updateFlow(ethers.BigNumber.from(Math.floor(parseInt(currentFlowRate) * 1.2)));
    }



  return (
    <div>
        <h2>Flow tool</h2>

        <h1>$ {ethers.utils.formatEther(currentBalance.toString())}</h1>

        <button onClick={() => {
            getFlowInfo();
        }}>Get flow information</button>

      <br/>

      <button onClick={() => {
        increaseFlow();
      }}>Increase flow
      </button>
    </div>

  );
};

export default UpdateFlow;
