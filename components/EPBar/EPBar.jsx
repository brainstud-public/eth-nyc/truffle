import React from 'react';
import classNames from 'classnames/bind';
import styles from './EPBar.module.css';
import { Card } from 'semantic-ui-react';
import { Indicator } from '../Indicator/Indicator';
import { useSuperFluid } from '../../providers/SuperFluidProvider/useSuperFluid';

const classes = classNames.bind(styles);

/**
 * EPBar.
 */
export const EPBar = ({ className, style }) => {
  const { effectiveness } = useSuperFluid();
  return (
    <Card fluid className={classes(styles.base, className)} style={style}>
      <div style={{ width: '100%' }}>
        <span className={styles.heading}>Learning Effectiveness</span>
        <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
          <Indicator active/>
          <div
            className={styles.bar}
            data-value={effectiveness}
            style={{ '--progress-width': `${effectiveness}%`, paddingLeft: `min(90%, ${effectiveness}%)` }}
          >
            <span style={{ position: 'absolute', zIndex: 2 }}>{effectiveness}%</span>
          </div>
        </div>
      </div>
    </Card>
  );
};
