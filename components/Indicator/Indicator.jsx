import React from 'react';
import classNames from 'classnames/bind';
import styles from './Indicator.module.css';
import { useSuperFluid } from '../../providers/SuperFluidProvider/useSuperFluid';

const classes = classNames.bind(styles);

/**
 * Indicator.
 */
export const Indicator = ({ className, style, children }) => {
  const { effectiveness } = useSuperFluid();

  const active = effectiveness > 50;

  return (
    <div className={classes(styles.base, className)} style={style}>
      <svg width="10" height="20" viewBox="0 0 10 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M5 0L9.33013 7.5H0.669873L5 0Z" className={classes(styles.arrow, { active })}/>
        <circle cx="5" cy="16" r="4" className={classes(styles.circle, { active: !active })}/>
      </svg>
    </div>
  );
};
