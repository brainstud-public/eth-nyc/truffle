import React from 'react';
import { Card } from 'semantic-ui-react';
import styles from './CourseCard.module.css';
import Link from 'next/link';


export const CourseCard = ({ id, image, children }) => {

  return (
    <Link href={`/courses/${id}`}>
      <a className={styles.base}>
        <Card style={{ backgroundImage: `url(${image}?v=${id})` }} className={styles['course-card']}>
          <Card.Content>
            {children}
          </Card.Content>
        </Card>
      </a>
    </Link>
  )

}
