import React from 'react';
import { Card } from 'semantic-ui-react';
import styles from './AssignmentCard.module.css';
import Link from 'next/link';
import { useRouter } from 'next/router';


export const AssignmentCard = ({ id, image, chapterId, children }) => {

  const { query } = useRouter();
  const { courseId } = query;

  return (
    <Link href={`/courses/${courseId}/chapters/${chapterId}/assignments/${id}`}>
      <a className={styles.base}>
        <Card style={{ backgroundImage: `url(${image}?v=${id})` }} className={styles['assignment-card']}>
          <Card.Content className={styles.content}>
            {children}
          </Card.Content>
        </Card>
      </a>
    </Link>
  )

}
