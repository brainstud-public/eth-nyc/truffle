import React from 'react';
import styles from './Ticker.module.css';
import classNames from 'classnames/bind';
import { Card } from 'semantic-ui-react';
import { Indicator } from '../Indicator/Indicator';
import { useSuperFluid } from '../../providers/SuperFluidProvider/useSuperFluid';

const classes = classNames.bind(styles);

/**
 * Ticker.
 */
export const Ticker = ({ active, finished, className, style }) => {
  const { balance, flowRate, isLoading } = useSuperFluid();

  return (
    <Card className={classes(styles.base, className)} style={style}>
      <div className={styles.text}>
        <span className={styles.heading}>{finished ? 'Final stream' : 'Current stream'}</span>
        <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
          <Indicator active={active} className={styles.indicator}/>
          <span>{isLoading ? 'Loading...' : `$${Math.round(balance / 1000000000) / 1000000000}`}</span>
        </div>
      </div>
    </Card>
  );
};
